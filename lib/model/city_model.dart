class CityModel {
  late int CityIds;
  late String CityNames;

  int get CityId => CityIds;

  CityModel({required this.CityNames , required this.CityIds});

  set CityId(int CityId) {
    CityIds = CityId;
  }

  String get CityName => CityNames;

  set CityName(String CityName) {
    CityNames = CityName;
  }
}