import 'dart:ui';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'first_screen.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: (AnimatedSplashScreen(
        splash: Image.asset('assets/images/splash.png.jpg'),
        nextScreen: FirstScreen(),
        splashTransition: SplashTransition.scaleTransition,
        splashIconSize: 150,
        duration: 3000,
        backgroundColor: Colors.white,
      )),
    );
  }
}