import 'package:flutter/material.dart';
import 'package:matrimony/matrimony/favourite_user.dart';
import 'package:matrimony/matrimony/female_user.dart';
import 'package:matrimony/matrimony/male_user.dart';
import 'package:matrimony/matrimony/user_list_page.dart';

class FirstScreen extends StatefulWidget {
  const FirstScreen({Key? key}) : super(key: key);

  @override
  State<FirstScreen> createState() => _FirstScreen();
}

class _FirstScreen extends State<FirstScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SafeArea(
              child: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.all(30.0),
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.fromLTRB(0, 10, 0, 60),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: const [
                          SizedBox(
                            width: 20,
                          ),
                          Text(
                            "DashBoard",
                            style: TextStyle(
                              color: Color.fromRGBO(34, 33, 91, 1),
                              fontSize: 16,
                              fontFamily: 'Gilroy-Semibold',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 10),
                      child: SizedBox(
                        width: double.infinity,
                        child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            elevation: 20,
                            color: const Color.fromRGBO(
                              34,
                              33,
                              91,
                              1,
                            ),
                            child: InkWell(
                              borderRadius: BorderRadius.circular(20),
                              splashColor: Colors.black54,
                              onTap: () {},
                              child: Padding(
                                  padding: const EdgeInsets.all(20),
                                  child: Stack(
                                    alignment: Alignment.topCenter,
                                    children: [
                                      Column(
                                        children: [
                                          Container(
                                            margin: const EdgeInsets.fromLTRB(
                                                0, 0, 0, 10),
                                            child: CircleAvatar(
                                              maxRadius: 25,
                                              child: Image.asset(
                                                'assets/images/me.png',
                                              ),
                                            ),
                                          ),
                                          Container(
                                            margin: const EdgeInsets.fromLTRB(
                                                0, 0, 0, 0),
                                            child: const Text(
                                              'Krisha Jagani',
                                              style: TextStyle(
                                                fontSize: 18,
                                                color: Colors.white,
                                                fontFamily: 'Gilroy-Semibold',
                                                fontWeight: FontWeight.w700,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            margin: const EdgeInsets.fromLTRB(
                                                0, 5, 0, 0),
                                            child: const Text(
                                              'Flutter Developer',
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 13,
                                                fontFamily: 'Gilroy-Light',
                                                fontWeight: FontWeight.w400,
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                10, 0, 10, 0),
                                            child: Container(
                                              margin: const EdgeInsets.fromLTRB(
                                                  0, 10, 0, 0),
                                              child: const Text(
                                                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ornare pretium placerat ut platea.',
                                                style: TextStyle(
                                                  fontSize: 13,
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 0.6),
                                                  fontFamily: 'Gilroy-Light',
                                                  fontWeight: FontWeight.w400,
                                                ),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                      Container(
                                        alignment: Alignment.topRight,
                                        child: Image.asset(
                                          'assets/images/pro.png',
                                          width: 40,
                                        ),
                                      )
                                    ],
                                  )),
                            )),
                      ),
                    ),
                    Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.fromLTRB(0, 100, 0, 30),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Expanded(
                                child: InkWell(
                                    borderRadius: BorderRadius.circular(20),
                                    splashColor: Color.fromRGBO(65, 94, 182, 1),
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                UserListPage()),
                                      );
                                    },
                                    child: buttons(
                                        Color.fromRGBO(238, 247, 254, 1),
                                        'assets/images/wedding.png',
                                        'All Users',
                                        Color.fromRGBO(65, 94, 182, 1))),
                              ),
                              SizedBox(
                                width: 9.5,
                              ),
                              Expanded(
                                child: InkWell(
                                  borderRadius: BorderRadius.circular(20),
                                  splashColor: Color.fromRGBO(255, 177, 16, 1),
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => MaleListPage()),
                                    );
                                  },
                                  child: buttons(
                                      Color.fromRGBO(255, 251, 236, 1),
                                      'assets/images/man.png',
                                      'Male Users',
                                      Color.fromRGBO(255, 177, 16, 1)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.fromLTRB(0, 0, 0, 9.5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Expanded(
                                child: InkWell(
                                    borderRadius: BorderRadius.circular(20),
                                    splashColor: Color.fromRGBO(172, 64, 64, 1),
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                FavouriteUserPage()),
                                      );
                                    },
                                    child: buttons(
                                        Color.fromRGBO(254, 238, 238, 1),
                                        'assets/images/favorite.png',
                                        'Favourite',
                                        Color.fromRGBO(172, 64, 64, 1))),
                              ),
                              SizedBox(
                                width: 9.5,
                              ),
                              Expanded(
                                child: InkWell(
                                    borderRadius: BorderRadius.circular(20),
                                    splashColor:
                                        Color.fromRGBO(35, 176, 176, 1),
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                FemaleUserPage()),
                                      );
                                    },
                                    child: buttons(
                                        Color.fromRGBO(240, 255, 255, 1),
                                        'assets/images/happy.png',
                                        'Female Users',
                                        Color.fromRGBO(35, 176, 176, 1))),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          )),
        ],
      ),
    );
  }

  Widget buttons(bgColor, imgAssets, Name, color) {
    return Card(
      elevation: 5,
      color: bgColor,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      child: Padding(
        padding: const EdgeInsets.all(0.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
              child: Image.asset(
                imgAssets,
                width: 55,
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Text(
                Name,
                style: TextStyle(
                  fontSize: 15,
                  color: color,
                  fontFamily: 'Gilroy-Light',
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
